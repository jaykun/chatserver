#pragma once

#ifndef CHATSERVER_H_
#define CHATSERVER_H_

#include <set>
#include <boost/asio.hpp>
#include "ChatLounge.h"

using boost::asio::ip::tcp;

class ChatServer
{
public:

	ChatServer(boost::asio::io_service &io_service, const tcp::endpoint &endpoint);

private:

	void AcceptConnections();

	tcp::acceptor acceptor;
	tcp::socket socket;
	std::shared_ptr<ChatLounge> lounge;
	std::set<std::shared_ptr<ChatRoom>> rooms;

	char message[512];
};

#endif
