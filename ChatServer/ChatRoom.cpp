#include "stdafx.h"
#include <iostream>
#include <string>
#include <set>
#include <boost/asio.hpp>
#include <boost/bind.hpp>

#include "ChatRoom.h"
#include "Chatter.h"

std::vector<std::shared_ptr<ChatRoom>> ChatRoom::rooms;

ChatRoom::ChatRoom()
{
	if (InstanceCount() <5)
	{		
		name = "Room" + std::to_string(++InstanceCount());
		cur_chatters = 0;
		max_chatters = 10;		
	}
}

void ChatRoom::Start()
{
	rooms.push_back(shared_from_this());
}

int& ChatRoom::InstanceCount()
{
	static int roomcount = 0;
	return roomcount;
}


void ChatRoom::Join(std::shared_ptr<Chatter> chatter)
{
	if (cur_chatters < max_chatters)
	{
		std::string msg = chatter->GetName() + " has joined the room";
		strcpy_s(data_to_send, msg.c_str());
		std::string chatters_in_room;

		decltype(chatters.size()) entry = 0;
		for (auto other_chatters : chatters)
		{
			if (other_chatters != chatter)
			{				
				if (entry == chatters.size()-1)
				{
					chatters_in_room += other_chatters->GetName();
				}
				else
				{
					chatters_in_room += other_chatters->GetName() + ",";
				}

				other_chatters->DeliverMsg(data_to_send);				
			}
			entry += 1;
		}

		msg = "You have joined " + name + ", other chatters here are: " + chatters_in_room;
		strcpy_s(data_to_send, msg.c_str());

		chatter->DeliverMsg(data_to_send);

		chatters.insert(chatter);
		cur_chatters += 1;
	}
	else
	{
		std::cout << "Unable to join, room full." << std::endl;
	}
}

void ChatRoom::Exit(std::shared_ptr<Chatter> chatter)
{
	std::string msg = chatter->GetName() + " has left the room";
	strcpy_s(data_to_send, msg.c_str());
	
	for (auto other_chatters : chatters)
	{
		if (other_chatters != chatter)
		{
			other_chatters->DeliverMsg(data_to_send);
		}
	}
	
	chatters.erase(chatter);
	cur_chatters -= 1;
}

void ChatRoom::Deliver(char *data, std::string nick)
{
	std::string msg = "<"+nick+">: " + data;
	strcpy_s(data_to_send, msg.c_str());
	for (auto chatter : chatters) 
	{
		chatter->DeliverMsg(data_to_send);
	}
}

std::string ChatRoom::RoomInfo()
{
	return (name + " users: " + std::to_string(cur_chatters) + "/" + std::to_string(max_chatters));		
}

std::string ChatRoom::GetName()
{
	return name;
}

std::shared_ptr<ChatRoom> ChatRoom::GetRoom(int roomnumber)
{
	return rooms[roomnumber-1];
}

