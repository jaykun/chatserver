#include "stdafx.h"
#include "ChatServer.h"

using boost::asio::ip::tcp;

ChatServer::ChatServer(boost::asio::io_service &io_service, const tcp::endpoint &endpoint) : acceptor(io_service, endpoint), socket(io_service)
{
	for (int i = 0; i < 5; ++i)
	{
		std::shared_ptr<ChatRoom> temp = std::make_shared<ChatRoom>();
		temp->Start();
		rooms.insert(temp);
	}

	lounge = std::make_shared<ChatLounge>();

	AcceptConnections();
}

void ChatServer::AcceptConnections()
{
	acceptor.async_accept(socket,
		[this](boost::system::error_code ec)
	{
		if (!ec)
		{
			std::make_shared<Chatter>(std::move(socket), lounge)->Start();
		}
		AcceptConnections();
	});
	
}

int main(int argc, char* argv[])
{
	try
	{
		if (argc < 2)
		{
			std::cout << "needs <port> as an argument" << std::endl;
			return 1;
		}

		boost::asio::io_service io_service;
		tcp::endpoint endpoint(tcp::v4(), std::atoi(argv[0]));

		ChatServer chatserver(io_service, endpoint);

		io_service.run();
	}
	catch (std::exception &e)
	{
		std::cerr << "Exception " << e.what() << std::endl;
	}

	return 0;
}
