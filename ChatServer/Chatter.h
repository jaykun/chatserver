#pragma once

#ifndef CHATTER_H_
#define CHATTER_H_

class ChatRoom;

#include <iostream>
#include <string>
#include <boost/asio.hpp>

using boost::asio::ip::tcp;

class Chatter
	: public std::enable_shared_from_this<Chatter>
{
public:
	Chatter(tcp::socket socket_, std::shared_ptr<ChatRoom> room_);
	~Chatter();

	void Start();

	void ContinueAfterInstructions(const std::string info, const std::string success, std::function<void()> Function);

	void SetNickname();

	void SetRoom();
	void ExitRoom();

	void ReceiveMsg();
	void DeliverMsg(char *data);

	tcp::socket& GetSocket();
	std::string GetSocketString();
	std::string GetName();

private:

	char data_in[512];
	char data_out[512];
	char nick[512];
	std::string nickname;
	tcp::socket socket;

	std::shared_ptr<ChatRoom> room;
};

#endif
