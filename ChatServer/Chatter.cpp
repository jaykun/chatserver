#include "stdafx.h"
#include <iostream>
#include <string>
#include <set>
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/lexical_cast.hpp>

#include "ChatRoom.h"
#include "Chatter.h"

Chatter::Chatter(tcp::socket socket_, std::shared_ptr<ChatRoom> room_) : socket(std::move(socket_)), room(room_)
{
	
}

Chatter::~Chatter()
{
	std::cout << "Removing chatter " << nickname << " from the server" << std::endl;
}

void Chatter::Start()
{
	room->Join(shared_from_this());
	
	std::string success = "Nickname requested from " + GetSocketString();
	std::function<void()> func = std::bind(&Chatter::SetNickname, shared_from_this());

	ContinueAfterInstructions("Please provide your nickname", success, func);
}

void Chatter::ContinueAfterInstructions(const std::string info, const std::string success, std::function<void()> Function)
{
	strcpy_s(data_out, info.c_str());

	auto self(shared_from_this());

	boost::asio::async_write(socket,
		boost::asio::buffer(data_out, sizeof(data_out)),
		[this, self,success,Function](boost::system::error_code ec, std::size_t length)
	{
		if (ec)
		{
			std::cout << ec.message() << std::endl;
			std::cout << "bytes transferred " << length << std::endl;
			ExitRoom();
		}
		else
		{
			std::cout << success << std::endl;
			Function();
		}
	});
}

void Chatter::SetNickname()
{
	auto self(shared_from_this());
	boost::asio::async_read(socket, boost::asio::buffer(data_in, sizeof(data_in)),
		[this, self](boost::system::error_code ec, size_t length)
	{
		if (ec)
		{
			std::cout << ec.message() << std::endl;
			std::cout << "bytes transferred " << length << std::endl;
			self->ExitRoom();
		}
		else
		{
			nickname = std::string(data_in);

			if (nickname == "")
			{
				std::cout << "empty name provided by " << GetSocketString() << std::endl;
				
				std::string success = "Nickname requested from " + GetSocketString();
				std::function<void()> func = std::bind(&Chatter::SetNickname, self);
				ContinueAfterInstructions("Please provide your nickname", success, func);
			}
			else
			{
				std::cout << "Name \"" << nickname << "\" accepted for " << GetSocketString() << std::endl;

				std::string roomlist = "Please select your room:\n";

				for (int i = 1; i <= ChatRoom::InstanceCount(); ++i)
				{
					roomlist += ChatRoom::GetRoom(i)->RoomInfo() + "\n";
				}

				std::string success = "Room number requested from " + nickname + " at " + GetSocketString();
				
				std::function<void()> func = std::bind(&Chatter::SetRoom, self);
				
				ContinueAfterInstructions(roomlist, success, func);

			}
		}
	});
}

void Chatter::SetRoom()
{
	auto self(shared_from_this());
	boost::asio::async_read(socket, boost::asio::buffer(data_in, sizeof(data_in)),
		[this, self](boost::system::error_code ec, size_t length)
	{
		if (ec)
		{
			std::cout << ec.message() << std::endl;
			std::cout << "bytes transferred " << length << std::endl;
			self->ExitRoom();
		}
		else
		{
					
			int number = std::atoi(data_in);
			std::cout << "Received Room number " << number << " from " << nickname << " at " << GetSocketString() << std::endl;

			//In case if unable to join room, set arguments for ContinueAfterInstructions(...) ready
			std::string roomlist = "Please select your room:\n";
			for (int i = 1; i <= ChatRoom::InstanceCount(); ++i)
			{
				roomlist += ChatRoom::GetRoom(i)->RoomInfo() + "\n";
			}
			std::string success = "Room number requested from " + nickname + " at " + GetSocketString();
			std::function<void()> func = std::bind(&Chatter::SetRoom, self);

			if (number > 0 && number < 6)
			{
				if (ChatRoom::GetRoom(number)->cur_chatters < ChatRoom::GetRoom(number)->max_chatters)
				{
					std::cout << nickname << " joined Room" << number << " with address: " << GetSocketString() << std::endl;
					ChatRoom::GetRoom(number)->Join(shared_from_this());
					room->Exit(shared_from_this());
					room = ChatRoom::GetRoom(number);;
					ReceiveMsg();							
				}
				else
				{
					std::cout << nickname << " failed joining full Room" << number << " " << ChatRoom::GetRoom(number)->cur_chatters << " / " << ChatRoom::GetRoom(number)->max_chatters << " with address: " << GetSocketString() << std::endl;
					ContinueAfterInstructions(roomlist, success, func);
				}
			}
			else
			{
				std::cout << nickname << " attempted to join non-existing Room " << "with address: " << GetSocketString() << std::endl;
				ContinueAfterInstructions(roomlist, success, func);
			}	
		}
	});
}

void Chatter::ExitRoom()
{
	std::cout << nickname << " Exiting " << room->GetName() << " with address: " << GetSocketString() << std::endl;
	room->Exit(shared_from_this());
	room = nullptr;
}

void Chatter::ReceiveMsg()
{
	auto self(shared_from_this());
	boost::asio::async_read(socket, boost::asio::buffer(data_in, sizeof(data_in)),
		[this, self](boost::system::error_code ec, size_t length)
	{
		if (ec)
		{
			std::cout << ec.message() << std::endl;
			std::cout << "bytes transferred " << length << std::endl;
			self->ExitRoom();
		}
		else
		{			
			room->Deliver(data_in,nickname);
			ReceiveMsg();
		}
	});
}

void Chatter::DeliverMsg(char *data)
{
	strcpy_s(data_out, data);

	auto self(shared_from_this());
	boost::asio::async_write(socket,
		boost::asio::buffer(data_out, sizeof(data_out)),
		[this, self, data](boost::system::error_code ec, std::size_t length)
	{
		if (ec)
		{
			std::cout << ec.message() << std::endl;
			std::cout << "bytes transferred " << length << std::endl;
			self->ExitRoom();
		}
		else
		{
			std::cout << "sent message \"" << data << "\" to " << self->GetName() << " at socket " << self->GetSocketString() << std::endl;
		}
	});
}

tcp::socket& Chatter::GetSocket()
{
	return socket;
}

std::string Chatter::GetName()
{
	return nickname;
}

std::string Chatter::GetSocketString()
{
	std::string s = boost::lexical_cast<std::string>(socket.remote_endpoint());
	return s;
}