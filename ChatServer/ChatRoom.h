#pragma once

#ifndef CHATROOM_H_
#define CHATROOM_H_

#include <iostream>
#include <string>
#include <set>
#include <boost/asio.hpp>
#include "Chatter.h"

using boost::asio::ip::tcp;

class ChatRoom
	: public std::enable_shared_from_this<ChatRoom>
{
public:
	ChatRoom();

	void Start();

	static int& InstanceCount();

	virtual void Join(std::shared_ptr<Chatter> chatter);

	virtual void Exit(std::shared_ptr<Chatter> chatter);

	void Deliver(char *data, std::string nick);

	std::string RoomInfo();

	std::string GetName();

	static std::shared_ptr<ChatRoom> GetRoom(int roomnumber);

	std::string name;
	int cur_chatters;
	int max_chatters;

protected:
	std::set<std::shared_ptr<Chatter>> chatters;

private:
	char data_to_send[512];

	static std::vector<std::shared_ptr<ChatRoom>> rooms;	
	//std::string password;
};

#endif
