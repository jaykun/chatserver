#pragma once

#ifndef CHATLOUNGE_H_
#define CHATLOUNGE_H_

#include "ChatRoom.h"

using boost::asio::ip::tcp;

class ChatLounge : public ChatRoom
{
public:
	ChatLounge();

	void Join(std::shared_ptr<Chatter> chatter);
};

#endif
