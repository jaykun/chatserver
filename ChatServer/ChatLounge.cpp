#include "stdafx.h"
#include "ChatLounge.h"


ChatLounge::ChatLounge()
{
	name = "Lounge";
}

void ChatLounge::Join(std::shared_ptr<Chatter> chatter)
{
	std::cout << "adding new user" << " to Lounge " << "at address " << chatter->GetSocketString() << std::endl;
	chatters.insert(chatter);
}
